# Overview

XSweet is divided into three separate repositories, which are grouped by their primary concerns:

## XSweet Core

XSweet Core is designed to extract data from MS Word, clean it up, and produce a good representation of the contents as HTML.

[Documentation](documentation/xsweet_core.md)

[Repository](https://gitlab.coko.foundation/XSweet/XSweet)

## HTMLevator

HTMLevator contains optional enhancements for the HTML, above and beyond simple extraction. This includes features such as plain text URL recognition and linking, heading inferring, copyediting cleanups, and more.

[Documentation](documentation/htmlevator.md)

[Repository](https://gitlab.coko.foundation/XSweet/HTMLevator)

## Editoria Typescript

Editoria Typescript transforms HTML to be loaded into the <a href="https://gitlab.coko.foundation/wax/wax">Wax</a> WYSIWYG word processor for <a href="https://editoria.pub/">Editoria</a>, where it can be styled, revised, and collaborated upon. This is a use-case-specific transformation chain, and a demonstration of how the HTML produced by XSweet Core and HTMLevator can be used as a pass-through format for conversions. Similar conversion chains can target other specific use cases in the same way.

[Documentation](documentation/editoria_typescript.md)

[Repository](https://gitlab.coko.foundation/XSweet/editoria_typescript)
